set output 'Timing4.svg
set terminal svg
set multiplot
set grid
set title "Clients scheduling latency"
set xlabel "audio cycles"
set ylabel "usec"
plot "profiler-test-case-5.log" using 9 title "jackplay/83" with lines, "profiler-test-case-5.log" using 17 title "output.pw-loopback-742/80" with lines, "profiler-test-case-5.log" using 25 title "my-sink-A/81" with lines, "profiler-test-case-5.log" using 33 title "jackplay/100" with lines, "profiler-test-case-5.log" using 41 title "output.pw-loopback-795/87" with lines, "profiler-test-case-5.log" using 49 title "my-sink-B/86" with lines, "profiler-test-case-5.log" using 57 title "alsa_output.platform-sound.stereo-fallback/32" with lines
unset multiplot
unset output
